package com.service.yltest2.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-03-07T07:40:30.757Z")

@RestSchema(schemaId = "yltest2")
@RequestMapping(path = "/yl-test2", produces = MediaType.APPLICATION_JSON)
public class Yltest2Impl {

    @Autowired
    private Yltest2Delegate userYltest2Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userYltest2Delegate.helloworld(name);
    }

}
